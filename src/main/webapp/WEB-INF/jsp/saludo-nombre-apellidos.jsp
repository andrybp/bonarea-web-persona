<%-- 
    Document   : saludo-nombre-apellido
    Created on : 05-oct-2021, 12:05:00
    Author     : andriy.blyshchak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hola Sr. ${persona.nombre} ${persona.apellidos} ${persona.dni}</h1>
        <h1>Vives en ${direccion.calle} ${direccion.poblacion} ${direccion.provincia}</h1>
    </body>
</html>
