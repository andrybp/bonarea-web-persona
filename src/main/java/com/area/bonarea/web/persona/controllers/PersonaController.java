/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.controllers;

import com.area.bonarea.web.persona.models.Direccion;
import com.area.bonarea.web.persona.models.Persona;
import com.area.bonarea.web.persona.services.PersonaService;
import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.ws.rs.BeanParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 *
 * @author andriy.blyshchak
 */

@Path("/saludo")
@Controller 
public class PersonaController {
    @Inject
    Models models;
    
    @Inject
    PersonaService personaService;
    
    
    @POST
    @Path("/nombre-apellidos")
    public String holaNombreApellidos(@BeanParam Persona persona,@BeanParam Direccion direccion){ 
        String result = "/WEB-INF/jsp/saludo-nombre-apellidos.jsp";
        models.put("persona",persona);
        models.put("direccion",direccion);
        personaService.add(persona,direccion);
        return result;
    }
}
