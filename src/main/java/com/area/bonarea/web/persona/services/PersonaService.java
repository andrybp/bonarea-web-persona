/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.services;

import com.area.bonarea.web.persona.models.Direccion;
import com.area.bonarea.web.persona.models.Persona;
import java.util.List;

/**
 *
 * @author andriy.blyshchak
 */
public interface PersonaService {
    Persona add(Persona persona,Direccion direccion);
    List<Persona> getAll();
}
