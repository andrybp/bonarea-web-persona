/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.services;

import com.area.bonarea.web.persona.dao.DireccionDao;
import com.area.bonarea.web.persona.dao.PersonaDao;
import com.area.bonarea.web.persona.models.Direccion;
import com.area.bonarea.web.persona.models.Persona;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author andriy.blyshchak
 */
public class PersonaServiceImpl implements PersonaService {

    @Inject
    PersonaDao personadao;

    @Inject
    DireccionDao direcciondao;

    @Override
    public Persona add(Persona persona, Direccion direccion) {
        persona = personadao.add(persona, personadao.crearConexio());
        direccion.setFk_Persona(persona.getId());
        persona.setDireccion(direccion);
        direcciondao.add(persona.getDireccion(), personadao.getConexio());
        return persona;
    }

    @Override
    public List<Persona> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
