/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.dao;

import com.area.bonarea.web.persona.models.Persona;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author andriy.blyshchak
 */
public interface PersonaDao {
    Persona add(Persona persona,Connection conn);
    List<Persona> getAll();
    Connection crearConexio();
    Connection getConexio();
}
