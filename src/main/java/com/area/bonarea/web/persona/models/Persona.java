/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.models;
import javax.ws.rs.FormParam;

/**
 *
 * @author andriy.blyshchak
 */
public class Persona {
    
    private Direccion direccion;
    
    private int id;
    
    @FormParam("nombre") 
    private String nombre;
    
    @FormParam("apellidos")
    private String apellidos;
    
    @FormParam("dni")
    private String dni;
    
    
    public Direccion getDireccion(){
        return direccion;
    }
    
    public void setDireccion(Direccion direccion){
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
