/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.dao;

import com.area.bonarea.web.persona.models.Direccion;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author andriy.blyshchak
 */
public interface DireccionDao {
    Direccion add(Direccion direccion,Connection conn); 
    List<Direccion> getAll();
}
