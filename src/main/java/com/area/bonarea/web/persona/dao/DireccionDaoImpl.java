/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.dao;

import com.area.bonarea.web.persona.models.Direccion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andriy.blyshchak
 */
public class DireccionDaoImpl implements DireccionDao {

    @Override
    public Direccion add(Direccion direccion,Connection conn) {
        try {
            PreparedStatement psInsert = conn.prepareStatement("INSERT INTO DIRECCION(calle,poblacion,provincia,fk_idPersona) VALUES(?,?,?,?)");

            conn.setAutoCommit(false);

            psInsert.setString(1, direccion.getCalle());
            psInsert.setString(2, direccion.getPoblacion());
            psInsert.setString(3, direccion.getProvincia());
            psInsert.setInt(4, direccion.getFk_Persona());

            psInsert.execute();

            conn.commit();
            conn.setAutoCommit(true);
            
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DireccionDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(DireccionDaoImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return direccion;
    }

    @Override
    public List<Direccion> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
