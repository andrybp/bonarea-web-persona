/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.bonarea.web.persona.dao;

import com.area.bonarea.web.persona.models.Persona;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andriy.blyshchak
 */
public class PersonaDaoImpl implements PersonaDao {
    
    private Connection conn;
    
    @Override
    public  Connection crearConexio(){
        //Definim conexio i els statements
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://eu-cdbr-west-01.cleardb.com:3306/heroku_cc4bf9307b6ca21?autoReconnect=true&useSSL=false","b8ea339fef24f7","f83e2f34");
        }catch(ClassNotFoundException | SQLException e){
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return this.conn;
    }

    @Override
    public Persona add(Persona persona,Connection conn) {
        try {
            PreparedStatement psInsert = this.conn.prepareStatement("INSERT INTO PERSONA(nombre,apellidos,dni) VALUES(?,?,?)",Statement.RETURN_GENERATED_KEYS);
            
            // Proces
            
            conn.setAutoCommit(false);
            
            psInsert.setString(1, persona.getNombre());
            psInsert.setString(2, persona.getApellidos());
            psInsert.setString(3, persona.getDni()); 
            
            psInsert.execute();
            
            //conn.setAutoCommit(true);
            
            ResultSet rs = psInsert.getGeneratedKeys();
            rs.next();
            persona.setId(rs.getInt(1));
            
            
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return persona;
    }

    @Override
    public List<Persona> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Connection getConexio() {
       return this.conn;
    }
    
}
